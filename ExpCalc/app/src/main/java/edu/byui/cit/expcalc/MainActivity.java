package edu.byui.cit.expcalc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView result;
    private EditText edit1, edit2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        edit1 = findViewById(R.id.edittext1);
        edit2 = findViewById(R.id.edittext2);
        result = findViewById(R.id.result);
        edit1.setSingleLine();
        edit2.setSingleLine();
        result.setSingleLine();
        result.setMaxWidth(420);

        Button btn_clear = findViewById(R.id.btn_clear);
        Calculate handler = new Calculate();
        edit1.addTextChangedListener(handler);
        edit2.addTextChangedListener(handler);


        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View mView) {
                edit1.setText("");
                edit2.setText("");
                result.setText("");
            }
        });
    }

    public class Calculate implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence mCharSequence, int mI, int mI1, int mI2) {

        }

        @Override
        public void onTextChanged(CharSequence mCharSequence, int mI, int mI1, int mI2) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String num1 = edit1.getText().toString();
            String num2 = edit2.getText().toString();
            if (s.length() > 0 && edit1.length() > 0) {
                try {
                    double editNum1 = Double.parseDouble(num1);
                    double editNum2 = Double.parseDouble(num2);
                    double mPow = Math.pow(editNum1, editNum2);
                    result.setText(String.valueOf(mPow));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }
    }
}
