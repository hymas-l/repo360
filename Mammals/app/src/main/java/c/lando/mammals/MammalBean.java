package c.lando.mammals;

public class MammalBean {

    private int legCount;
    private String color;
    private double height;

    MammalBean(int legCount, String color, double height) {
        this.legCount = legCount;
        this.color = color;
        this.height = height;
    }

    int getLegCount() {
        return legCount;
    }

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return super.toString() + ". The Dog has " + getLegCount() + " legs. " + "The color of the " +
                "dog is " + getColor() + ". The height of the dog is " + getHeight() + ".";
    }
}
