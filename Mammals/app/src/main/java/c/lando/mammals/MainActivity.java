package c.lando.mammals;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public static void main() {
        Kennel mKennel = new Kennel();
        DogBean[] allDogs;
        allDogs = mKennel.buildDogs();
        mKennel.displayDogs(allDogs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        main();
    }
}
