package c.lando.mammals;


public class Kennel {

    public DogBean[] buildDogs() {
        DogBean[] mDogBeans = new DogBean[5];
        mDogBeans[0] = new DogBean("Penguin", "Icey", 2, "White and Black", 26.64);
        mDogBeans[1] = new DogBean("Weiner Dog", "Long Body", 4, "Brown", 10.23);
        mDogBeans[2] = new DogBean("Dinosaur", "Barney", 2, "Purple", 58.62);
        mDogBeans[3] = new DogBean("Poodle", "Noodle", 4, "Black", 12.54);
        mDogBeans[4] = new DogBean("Bulldog", "Beast", 4, "Pink", 16.26);

        return mDogBeans;
    }

    public void displayDogs(DogBean[] mDogBeans) {
        for (int x = 0; x < 5; x++) {
            System.out.println(mDogBeans[x].toString());
        }
    }
}
