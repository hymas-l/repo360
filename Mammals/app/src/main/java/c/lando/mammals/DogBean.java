package c.lando.mammals;

public class DogBean extends MammalBean{

    private String breed;
    private String name;

    DogBean(String breed, String name, int legCount, String color, Double height) {
        super(legCount, color, height);
        this.breed = breed;
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString() + "The dog's breed is " + breed + ". The dog's name is " + name;
    }
}
