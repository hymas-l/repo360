package c.lando.mammals;

import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

public class MammalBeanTest {

    @Test
    public void getLegCount() {
        int legTest = 4;
        double height =  24.3;
        String color = "brown";
        MammalBean mMammal1 = new MammalBean(legTest, color, height);
        Set<MammalBean> listOfMammals = new TreeSet<MammalBean>();
        listOfMammals.add(mMammal1);
        assertEquals(legTest, mMammal1.getLegCount());
    }

}