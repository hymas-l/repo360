package com.lando.collections;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.TreeMap;


public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView console = findViewById(R.id.console);
        System.setOut(new PrintStream(new TextViewWriter(console)));
    }

    private static final class TextViewWriter extends OutputStream {
        private final StringBuilder buffer;
        private final TextView console;

        TextViewWriter(TextView console) {
            this.buffer = new StringBuilder();
            this.console = console;
        }

        @Override
        public void write(int b) {
            buffer.append(b);
            console.setText(buffer);
        }

        @Override
        public void write(@NotNull byte[] b, int offs, int len) {
            buffer.append(new String(b, offs, len));
            console.setText(buffer);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        main();
    }


    private void main() {

        TreeMap<String, Account> acc = new TreeMap<>();

        try {
            // Read an existing file from the assets folder.
            String[] read = readFromAssets("rawData.csv");

            // Print the text that was read from the text file.
            for (String line : read) {
                System.out.println(line);
            }

            // Write a text file to internal storage. Then
            // read that same file from internal storage.


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String[] readFromAssets(String filename) throws IOException {
        // Create an ArrayList to hold the lines of text from the file.
        TreeMap<String, Account> accounts = new TreeMap();

        BufferedReader br = null;
        InputStreamReader ir = null;
        InputStream is = getAssets().open(filename);
        try {
            ir = new InputStreamReader(is);
            br = new BufferedReader(ir);

            // Read each line from the file and add each line to the ArrayList.
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.trim().split(",");
                if (!accounts.containsKey(parts[0])) {
                    Account act1 = new Account(parts[0], parts[1], parts[2], parts[3]);
                    accounts.put(parts[0], act1);
                    System.out.println(accounts.get(parts[0]));
                }
            }
        } finally {
            // Close the outermost reader that was successfully opened.
            if (br != null) {
                br.close();
            } else if (ir != null) {
                ir.close();
            } else {
                is.close();
            }
        }

        int size = accounts.size();
        String[] array = new String[size];


        return null;
    }
}